Import("env")
# access to global construction environment
print(env)
# access to project construction environment

my_flags = env.ParseFlags(env['BUILD_FLAGS'])
defines = {k: v for (k, v) in my_flags.get("CPPDEFINES")}
print(defines)
env.Replace(PROGNAME="firmware_ubibot_%s" % defines.get("VERSION")) 