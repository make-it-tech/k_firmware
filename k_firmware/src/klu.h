#ifndef KLU_H
#define KLU_H

#include "klugit_config.h"
//#include <ModbusRtu.h>


void KluInit();
void KluTasks();

float KluGetTemperature();
float KluGetWaterVolume();
void CheckKluActive();
bool is_KluReadingMsg();
bool is_kluPresent();
void KluGetMsg();

bool is_KluActive();



#endif