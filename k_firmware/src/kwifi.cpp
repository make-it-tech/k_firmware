#include "kwifi.h"

//#define CREDENTIALS
WiFiManager wifiManager;

//NTP Client
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org");

unsigned long attemps = WIFI_CONNECTION_ATTEMPS_S *1000;
unsigned long connection_time = 0;
unsigned long last_connection = 0;
unsigned long last_online_time = 0;

static bool wakeup_by_factory_reset = false;

wifi_event_t wifi_event;
wifi_state_t wifi_state = WIFI_STATE_OFFLINE;

//PRIVATE PROTOTYPES
static void WifiForceConnection();
static void WifiOfflineTasks();
static void WifiOnlineTasks();
static void WifiParingTasks();
static void WifiConnectedTasks();
static void WifiDisconnectedTasks();
static void WifiSetState(wifi_state_t state);
static void WiFiStationDisconnected(WiFiEvent_t event, WiFiEventInfo_t info);
static void WiFiStationConnected(WiFiEvent_t event, WiFiEventInfo_t info);
static void WifiStartAP();

void WifiInit()
{   
    static unsigned long start_time = millis();
    static unsigned long now = 0;

    WiFi.onEvent(WiFiStationConnected, SYSTEM_EVENT_STA_CONNECTED);
    WiFi.onEvent(WiFiStationDisconnected, SYSTEM_EVENT_STA_DISCONNECTED);
    
    //#define CREDENTIALS
    #ifdef CREDENTIALS
        WiFi.begin("MAKE_IT_mobile", "MIcompany2020");        
        while (WiFi.status() != WL_CONNECTED)
        {
            delay(500);
            Serial.print(".");
        }
        Serial.println();
        Serial.print("Connected, IP address: ");
        Serial.println(WiFi.localIP());
        wifi_event = WIFI_EVENT_CONNECTED;
        
    #else
        WiFi.begin();

        if(WiFi.SSID().length() != 0)
        {   
            //try to connect to a known wifi
            while (WiFi.status() != WL_CONNECTED)
            {
                now = millis();
                if (now - start_time > WIFI_STA_CON_TIMEOUT_S*1000)
                {
                    ESP.restart();
                }
                HmiTasks();
            }
            //how long it takes to connect to Wifi
            connection_time = now - start_time;

            DEBUG_PRINT_LN("Conected");
            wakeup_by_factory_reset = false;
            wifi_event = WIFI_EVENT_PARED;
        }
        else
        {
            WifiSetState(WIFI_STATE_PARING);
            DEBUG_PRINT_LN("Can't connect to any known Wifi");
            WifiStartAP();
            wakeup_by_factory_reset = true;
        
        } 
    #endif
    
    timeClient.begin();
}

void WifiTasks()
{
    wifiManager.process();

    static wifi_state_t previous_state = wifi_state;
    bool print_state = false;

    if(previous_state != wifi_state)
        {
            print_state = true;
        }
    previous_state = wifi_state;

    switch (wifi_state)
    {
    case WIFI_STATE_PARING:
        WifiParingTasks();
        if(print_state)
        DEBUG_PRINT_LN("##__##-STATE_PARING");
        break;

    case WIFI_STATE_OFFLINE:
        WifiOfflineTasks();
        if(print_state)
        DEBUG_PRINT_LN("##__##-STATE_OFFLINE");
        break;

    case WIFI_STATE_ONLINE:
        WifiOnlineTasks();
        if(print_state)
        DEBUG_PRINT_LN("##__##-STATE_ONLINE");
        break;
    
    case WIFI_STATE_CONNECTED:
        WifiConnectedTasks();
        if(print_state)
        DEBUG_PRINT_LN("##__##-STATE_CONNECTED");
        break;

    case WIFI_STATE_DISCONNECTED:
        WifiDisconnectedTasks();
        if(print_state)
        DEBUG_PRINT_LN("##__##-STATE_DISCONNECTED");
        break;
    
    default:
        break;
    }

    //Reset events
    if (wifi_event != WIFI_EVENT_NONE)
	{
		wifi_event = WIFI_EVENT_NONE;
	}

    //wifiManager.process();
}

void WifiStartAP()
{
    HmiSetOrange();
    EnergyPowerON();
    // Local intialization. Once its business is done, there is no need to keep it around
    DEBUG_PRINT_LN("Starting ap mode");
    Serial.setDebugOutput(true);
    //to allow led animation AP config must be non-blocking
    wifiManager.setConfigPortalBlocking(true);
    //wifiManager.setConfigPortalTimeout(WIFI_AP_PORTAL_TIMEOUT_S);   
    wifiManager.setMinimumSignalQuality(WIFI_AP_SCAN_MIN_SIGNAL);
    wifiManager.setAPStaticIPConfig(IPAddress(34,252,69,88), IPAddress(34,252,69,88), IPAddress(255,255,255,0));   
    //wifiManager.setShowPassword(true);
    wifiManager.autoConnect(WIFI_SSID);
    MiscReset();
}

unsigned long WifiGetEpoch()
{
    timeClient.update();
    unsigned long epochTime = timeClient.getEpochTime();
    // Serial.print("Epoch Time: ");
    // Serial.println(epochTime);
    return epochTime;
}

void WifiOnlineTasks()
{
    Mqtt_Connect();
    last_online_time = millis();
    //ping server to evaluate internet connection
    if(!WifiPingServer())
    {
        //no internet connection
        wifi_event = WIFI_EVENT_NO_PING;
    }

    // Handle events
    if (wifi_event == WIFI_EVENT_DISCONNECTED)
    {
        WifiSetState(WIFI_STATE_DISCONNECTED);
        return;
    }
    else if (wifi_event == WIFI_EVENT_FACTORY_RESET)
    {
        WifiSetState(WIFI_STATE_PARING);
        return;
    }
    else if (wifi_event == WIFI_EVENT_NO_PING)
    {
        WifiSetState(WIFI_STATE_OFFLINE);
        return;
    }
}

void WifiOfflineTasks()
{
    //Connected to the Router but without internet connection
    //WifiForceConnection();
    static bool first_time = true;

    if (first_time)
    {
        first_time = !first_time;
        last_online_time = millis();
    }

    if(millis() - last_online_time > WIFI_OFFLINE_TIMEOUT_S*1000)
    {
        //not connected for more than X seconds
        DEBUG_PRINT_LN("Reseting by Offline timeout");
        MiscReset();
    }

    if(WifiPingServer())
    {
        //no internet connection
        wifi_event = WIFI_EVENT_PING;
    }

    // Handle events
    if (wifi_event == WIFI_EVENT_DISCONNECTED)
    {
        WifiSetState(WIFI_STATE_DISCONNECTED);
        return;
    }
    else if (wifi_event == WIFI_EVENT_PING)
    {
        WifiSetState(WIFI_STATE_ONLINE);
        return;
    }
    else if (wifi_event == WIFI_EVENT_FACTORY_RESET)
    {
        WifiSetState(WIFI_STATE_PARING);
        return;
    }
    
}

void WifiParingTasks()
{   
    //close relay
    // if (wifi_event == WIFI_EVENT_CONNECTED)
    // {
    //     WifiSetState(WIFI_STATE_ONLINE);
    // }

    // Handle events
    if (wifi_event == WIFI_EVENT_PARED)
    {
        WifiSetState(WIFI_STATE_CONNECTED);
        return;
    }

    //this transition is not in the state machine diagram... sometimes 
    if (wifi_event == WIFI_EVENT_DISCONNECTED)
    {
        WifiSetState(WIFI_STATE_DISCONNECTED);
        return;
    }
}

void WifiConnectedTasks()
{
    //Other events generated in the callbacks
    if(WifiPingServer())
    {
        wifi_event = WIFI_EVENT_PING;
    }
    else
    {
        wifi_event = WIFI_EVENT_NO_PING;
    }

    // Handle events
    if (wifi_event == WIFI_EVENT_DISCONNECTED)
    {
        WifiSetState(WIFI_STATE_DISCONNECTED);
        return;
    }
    else if (wifi_event == WIFI_EVENT_PING)
    {
        WifiSetState(WIFI_STATE_ONLINE);
        return;
    }
    else if (wifi_event == WIFI_EVENT_NO_PING)
    {
        WifiSetState(WIFI_STATE_OFFLINE);
        return;
    }
    else if (wifi_event == WIFI_EVENT_FACTORY_RESET)
    {
        WifiSetState(WIFI_STATE_PARING);
        return;
    }
}

void WifiDisconnectedTasks()
{
    WifiForceConnection();
    // Handle events
    if (wifi_event == WIFI_EVENT_CONNECTED)
    {
        WifiSetState(WIFI_STATE_CONNECTED);
        return;
    }
    else if (wifi_event == WIFI_EVENT_FACTORY_RESET)
    {
        WifiSetState(WIFI_STATE_PARING);
        return;
    }
    WifiForceConnection();
}


void WiFiStationConnected(WiFiEvent_t event, WiFiEventInfo_t info)
{
    Serial.println("Connected to AP successfully!");
    wifi_event = WIFI_EVENT_CONNECTED;
}

void WiFiStationDisconnected(WiFiEvent_t event, WiFiEventInfo_t info)
{
  Serial.println("Disconnected from WiFi access point");
  Serial.print("WiFi lost connection. Reason: ");
  Serial.println(info.disconnected.reason);
  wifi_event = WIFI_EVENT_DISCONNECTED;
  DEBUG_PRINT_LN(wifi_state);
}

void WifiSetState(wifi_state_t state)
{
	wifi_state = state;
}

wifi_state_t WifiGetState(void)
{
	return wifi_state;
}

void WifiEraseCredentials()
{
    DEBUG_PRINT_LN("Erasing wifi credentials");
    wifiManager.resetSettings();
    //HMI_EpromErase();
    DEBUG_PRINT_LN("Reseting (3)");
    ESP.restart();
}

bool WifiWakeFromFactoryReset()
{
    return wakeup_by_factory_reset;
}

unsigned long WifiConnectionTime()
{
    return connection_time;
}

bool WifiPingServer()
{
    static unsigned long last_check = 0;
    static bool success = false;
    static bool last_ping_success= false;
    static uint8_t counter = 0;

    if(millis()-last_check > 5000 
    || WifiGetState() == WIFI_STATE_OFFLINE &&  millis()-last_check > 3000)
    {
        // DEBUG_PRINT("Ping counter: ");
        // DEBUG_PRINT_LN(counter); 
        // DEBUG_PRINT("PING : ");
        success = Ping.ping(KLUGIT_AWS_MQTT_HOST, 1);
        last_check = millis();
        DEBUG_PRINT_LN(success);

        if(!success)
            {
                if(counter == 1)
                {
                    last_ping_success = false;
                }
                counter ++;
            }    
            else
            {
                counter = 0;
                last_ping_success = true;
            }
    }
    return last_ping_success;
    
    
}

void WifiForceConnection()
{
    static bool is_connecting = false;
    static unsigned long offline_start_time = 0;

    //Force connection
    if (WiFi.status() != WL_CONNECTED)
    {
        if(is_connecting)
        {   
            //reset it it takes to long to connect
            if(millis()-offline_start_time > WIFI_DISCONNECTED_TIMEOUT_S*1000)
            {
                DEBUG_PRINT_LN("Reseting for timeout trying to connect");
                ESP.restart();
            }
        }
        else
        {
            offline_start_time = millis();
            WiFi.disconnect();
            WiFi.begin();
            is_connecting = true;
        }
    }

    else
    {
        DEBUG_PRINT_LN("Connected");
        //wifi_event = WIFI_EVENT_CONNECTED;
        is_connecting = false;
    }
}