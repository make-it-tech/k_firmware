#include "klu.h"
#include "../lib/Modbus-Master-Slave-for-Arduino-master/ModbusRtu.h"

uint16_t au16data[4]; //!< data array for modbus network sharing
uint8_t u8state; //!< machine state
uint8_t u8query; //!< pointer to message query

/**
 *  Modbus object declaration
 *  u8id : node id = 0 for master, = 1..247 for slave
 *  port : serial port
 *  u8txenpin : 0 for RS-232 and USB-FTDI 
 *               or any pin number > 1 for RS-485
 */
Modbus master(0,Serial2,0); // this is master and RS-232 or USB-FTDI

/**
 * This is an structe which contains a query to an slave device
 */
modbus_t telegram[2];
uint16_t counter = 0;
bool reading_msg = false;
bool reading_complete = false;

unsigned long new_reading_start_ts = 0;

unsigned long u32wait;
bool klu_is_present = false;
uint16_t last_reading_temp   = 0.0;
uint16_t last_reading_flow   = 0.0;

bool klu_is_active = KLU_IS_ACTIVE;

void KluInit()
{
    Serial2.begin(4800, SERIAL_8N1, 16, 17);
    
    master.start();
    master.setTimeOut(1000); // if there is no answer in 5000 ms, roll over
    u32wait = millis() + 1000;
    u8state = u8query = 0; 
    
    pinMode(26, OUTPUT);
    digitalWrite(26, 0);

}

void KluTasks()
{
    static unsigned long last_check = 0;
    static unsigned long last_message = 0;
    unsigned long now = millis();

    if (now - last_check > 10000)
    {
        //check if KLU is present
        last_check = now;
        CheckKluActive();       
    }    

    if(now - last_message > 2000)
    {   
        // read while 
        if(reading_msg)
        {
            KluGetMsg();

            if(!reading_msg)
            {
                
            }
        }

        if(!reading_msg)
        {
            reading_msg = true;
            new_reading_start_ts = now;
            //root serial to modbus chip
            digitalWrite(26, 0);
        }
              
        
        if(!reading_msg)
        {
            last_message = millis();
            digitalWrite(26, 1);
        }
    }
}

void KluGetMsg()
{
    ///read messages
    au16data[0] = 0;

    // telegram 0: read registers
    telegram[0].u8id = 1; // slave address
    telegram[0].u8fct = 3; // function code (this one is registers read)
    telegram[0].u16RegAdd = 0; // start address in slave
    telegram[0].u16CoilsNo = 4; // number of elements (coils or registers) to read
    telegram[0].au16reg = au16data; // pointer to a memory array in the Arduino

    switch( u8state ) 
    {
        case 0: 
            if (millis() > u32wait) u8state++; // wait state
            break;

        case 1: 
            master.query(telegram[0]); // send query (only once)
            u8state++;
            break;

        case 2:        
            master.poll(); // check incoming messages
            if (master.getState() == COM_IDLE) 
            {             
                u8state = 0;
                u32wait = millis() + 100; 
            }

            //was it a new msg?
            if(au16data[0] != 0)
            {
                reading_msg = false;
                klu_is_present = true;

                //Print the shared array
                for (int i = 0; i < 4; i++)
                {
                    DEBUG_PRINT(au16data[i]);
                    DEBUG_PRINT("-"); 
                }
                Serial.println("");

                last_reading_flow = au16data[1];
                last_reading_temp = au16data[0];
                //reset flow
            }
            break;
    }
    if(millis() - new_reading_start_ts > 300)
    {
        //force timeout
        reading_msg = false;
    }
}

float KluGetTemperature()
{
    static float temperature = 0.0;
    
    if(!is_KluActive())
    {
        //Is KLU is desactivated return 0 as temperature
        return 0;
    }    

    if(last_reading_temp != 0)
    {
        temperature = float(last_reading_temp) / 100.0;
        if(temperature >-10.0 && temperature < 120.0)
        {
            return temperature;
        }
        else
        {
            return 0;
        } 
    } 
}

float KluGetWaterVolume()
{
    static float water_volume = 0;
    water_volume = float(last_reading_flow) / 100.0;

    if(!is_KluActive())
    {
        //Is KLU is desactivated return 0 as WATER VOLUME
        return 0;
    }   
    //reset the value of the slave
    //the water volume is comulative, every time one reads the value must reset it
    telegram[1].u8id = 1; // slave address
    telegram[1].u8fct = 6; // function code (this one is write a single register)
    telegram[1].u16RegAdd = 1; // start address in slave
    telegram[1].u16CoilsNo = 1; // number of elements (coils or registers) to read
    telegram[1].au16reg = au16data+2; // pointer to a memory array in the Arduino
    master.query(telegram[1]);

    return water_volume;
}

bool is_kluPresent()
{
    return klu_is_present;
}

bool is_KluActive()
{
    return klu_is_active;
}

void CheckKluActive()
{
    static uint16_t previous_counter = 0;
    
    //last received coutner
    counter = au16data[3];

    if(previous_counter == counter)
    {
        //KLU not present
        DEBUG_PRINT_LN("KLU sensor not present");

        klu_is_present = false;
    }
    else
    {
        previous_counter = counter;
        klu_is_present = true;
    }
}

bool is_KluReadingMsg()
{
    return reading_msg;
}