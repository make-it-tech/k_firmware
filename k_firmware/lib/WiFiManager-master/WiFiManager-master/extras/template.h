const char HTTP_HEADER[] PROGMEM          = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\"/><title>{v}</title>";
const char HTTP_STYLE[] PROGMEM            = "<style> body{background: #edf7ff;} .conf_ssid{line-height: 21px; color: #55616B; text-decoration: none; } .conf_input{background: #FFFFFF; text-decoration: none; border-style: none; box-shadow: 0px 10px 40px rgba(9, 38, 66, 0.05); border-radius: 5px; margin: 10px 0px; padding: 10px 5px;} .conf_b{background: #62B5C3; padding: 5px 5px; text-decoration: none; border-radius: 100px; box-shadow: 0px 0px 30px rgba(98, 181, 195, 0.3);} .c{text-align: center;} div,input{padding:5px;font-size:1em;} input{width:95%;} body{text-align: center;font-family: 'Verdana', cursive;} button{border:0;border-radius:0.3rem;background-color:#1fa3ec;color:#fff;line-height:2.4rem;font-size:1.2rem;width:100%;} .q{float: right;width: 64px;text-align: right;} .l{background: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAALVBMVEX///8EBwfBwsLw8PAzNjaCg4NTVVUjJiZDRUUUFxdiZGSho6OSk5Pg4eFydHTCjaf3AAAAZElEQVQ4je2NSw7AIAhEBamKn97/uMXEGBvozkWb9C2Zx4xzWykBhFAeYp9gkLyZE0zIMno9n4g19hmdY39scwqVkOXaxph0ZCXQcqxSpgQpONa59wkRDOL93eAXvimwlbPbwwVAegLS1HGfZAAAAABJRU5ErkJggg==\") no-repeat left center;background-size: 1em;}</style>";

const char HTTP_SCRIPT[] PROGMEM          = "<script>function c(l){document.getElementById('s').value=l.innerText||l.textContent;document.getElementById('p').focus();}</script>";
const char HTTP_HEADER_END[] PROGMEM        = "</head><body class='class_b'><div style='text-align:left;display:inline-block;min-width:260px;'>";
const char HTTP_PORTAL_OPTIONS[] PROGMEM  = "<form action=\"/wifi\" method=\"get\"><button class='conf_b'>Configure WiFi</button></form><br/><form action=\"/0wifi\" method=\"get\"><button class='conf_b'>Configure WiFi (No Scan)</button></form><br/><form action=\"/i\" method=\"get\"><button class='conf_b'>Info</button></form><br/><form action=\"/r\" method=\"post\"><button class='conf_b'>Reset</button></form>";
const char HTTP_ITEM[] PROGMEM            = "<div><a class = 'conf_ssid' href='#p' onclick='c(this)'>{v}</a>&nbsp;<span class='q {i}'>{r}%</span></div>";

const char HTTP_FORM_START[] PROGMEM      = "<form method='get' action='wifisave'><input class='conf_input' id='s' name='s' length=32 placeholder='SSID'><br/><input class='conf_input' id='p' name='p' length=64 type='password' placeholder='password'><br/>";
const char HTTP_FORM_PARAM[] PROGMEM      = "<br/><input id='{i}' name='{n}' maxlength={l} placeholder='{p}' value='{v}' {c}>";

const char HTTP_FORM_END[] PROGMEM        = "<br/><button class='conf_b' type='submit'>connect</button></form>";
const char HTTP_SCAN_LINK[] PROGMEM       = "<br/><a href=\"/wifi\"><button class='conf_b'>scan</button></a>";

const char HTTP_SAVED[] PROGMEM           = "<div>Saved<br />Trying to connect Klugit Plug to network.<br /></div>";
const char HTTP_END[] PROGMEM             = "</div></body></html>";
