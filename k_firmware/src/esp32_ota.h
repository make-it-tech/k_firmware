#ifndef ESP32_OTA_H
#define ESP32_OTA_H


#include <esp32fota.h>
#include <WiFi.h>

void esp32_OtaUpdate(const char* add);

#endif