#ifndef HMI_H
#define HMI_H

#include "klugit_config.h"
#include <EasyButton.h>
#include "../lib/singleLEDLibrary-master/singleLEDLibrary-master/singleLEDLibrary.h"
#include "Mqtt.h"
#include "kwifi.h"

#define HMI_LED_GREEN       13
#define HMI_BUTTON          34
#define HMI_LED_BLUE        33
#define HMI_LED_BREATH_TIME 2000
#define HMI_LED_BREATH_TIME_PARING  500
#define HMI_LED_BREATH_TIME_OFFLINE 2000
#define HMI_LONG_PRESS      8000

enum leds {blue, orange};  

typedef enum {
    HMI_STATE_OFF = 0,
    HMI_STATE_SM_ON,
    HMI_STATE_SM_OFF,
    HMI_STATE_HEATING_NOW

}HMI_state_t;    

void HmiInit();
void HmiTasks();
void HmiLedON(leds user_led);
void HmiLedOFF(leds user_led);
void HmiLedToggle();
void HmiSetOrange();

static void HmiOffTasks();
static void HmiSmartModeOnTasks();
static void HmiSmartModeOffTasks();
static void HmiHeatingNowTasks();
static void HmiOfflineTasks();
static void HmiParingTasks();

void buttonPressed();
void buttonLongPress();
void buttonISR();
void HmiSetState(HMI_state_t state);
HMI_state_t HmiGetState(void);

#endif