#ifndef SENSORS_H
#define SENSORS_H

#include "ds18b20.h"
#include "klu.h"

#define KLUGIT

void SensorsInit();
void SensorsTasks();
float SensorsGetPipeTemp();
float SensorsGetHotTemp();
float SensorsGetWaterVolume();

#endif