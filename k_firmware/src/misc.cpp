#include "misc.h"

void MiscReset()
{
    DEBUG_PRINT_LN("ESP Will perforn an Reset");
    ESP.restart();
}

void MiscRestart()
{
    DEBUG_PRINT_LN("ESP Will perforn an Restart");
    ESP.restart();
}