#ifndef KLUGIT_CONFIG_H
#define KLUGIT_CONFIG_H

#include <Arduino.h>
#define FW_VERSION  "0.91"

#define INTERNAL_TEMP
#define EXTERNAL_TEMP

#define MQTT_TOPIC_SIZE 35

#define KLUGIT_AWS_INFLUX_HOST      "http://klugit.iot.klugitenergy.com:3033"
#define KLUGIT_AWS_INFLUX_PORT      "3033"
#define KLUGIT_AWS_MQTT_HOST        "iot.klugitenergy.com"
#define KLUGIT_AWS_MQTT_PORT        1888


#define INFO_BACKOFFICE_RESET   1
#define INFO_ERROR_I2C_HANG     2
#define INFO_RELAY_OVERTEMP     3
#define INFO_OTA_UPDATE         4
#define INFO_BACKOFFICE_ERASE   5
#define INFO_MQTT_NO_CONNECTION 6
#define INFO_BACKOFFICE_RESTART 7

#define KLU_IS_ACTIVE           0

#define RELAY_CLOSE_ON_OFFLINE

#define DEBUG 1

#ifdef DEBUG
    #define DEBUG_PRINT(x) Serial.print(x)
    #define DEBUG_PRINT_LN(x) Serial.println(x)
#else
    #define DEBUG_PRINT(x)
    #define DEBUG_PRINT_LN(x)
#endif
#endif