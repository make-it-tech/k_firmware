#include "hmi.h"

bool led_on = false;

EasyButton button(HMI_BUTTON);
HMI_state_t hmi_state = HMI_STATE_OFF;
HMI_state_t hmi_previous_state = HMI_STATE_OFF;
sllib led_blue(HMI_LED_BLUE);
sllib led_orange(HMI_LED_GREEN);

static bool button_pressed = false;
static bool button_long_pressed = false;

void HmiInit()
{
    uint32_t long_press_time = HMI_LONG_PRESS;
    button.begin();
    button.onPressed(buttonPressed);
    button.onPressedFor(long_press_time, buttonLongPress);
    button.enableInterrupt(buttonISR);

}
void HmiTasks()
{
    if(WifiGetState() == WIFI_STATE_OFFLINE || WifiGetState()== WIFI_STATE_DISCONNECTED || WifiGetState()== WIFI_STATE_CONNECTED)
    {
        //if there is no internet connection
        HmiOfflineTasks();
    }

    else if (WifiGetState() == WIFI_STATE_PARING)
    {
        HmiParingTasks();
    }
    else
    {
        switch (hmi_state)
        {
        case HMI_STATE_OFF:
            HmiOffTasks();
            break;

        case HMI_STATE_SM_ON:
            HmiSmartModeOnTasks();
            break;

        case HMI_STATE_SM_OFF:
            HmiSmartModeOffTasks();
            break;

        case HMI_STATE_HEATING_NOW:
            HmiHeatingNowTasks();
            break;

        default:
            //do something;
            break;
        }
    }
    button.update();
    led_blue.update();
    led_orange.update();

    if(button_pressed)
    {
        Serial.println("Button pressed");
        MqttSendButtonEvent(false);
        button_pressed = !button_pressed;
    }
    else if (button_long_pressed)
    {
        Serial.println("Button pressed for two seconds");
        led_orange.setBreathSingle(HMI_LED_BREATH_TIME/4);
        MqttSendButtonEvent(true);
        button_long_pressed = !button_long_pressed;
        WifiEraseCredentials();
    }
    
    

    // if (hmi_event != HMI_EVENT_NONE)
	// {
	// 	hmi_event = HMI_EVENT_NONE;
	// }
    
}

void HmiLedToggle()
{
    if(led_on)
    {
        //HmiLED_OFF();
    }
    else
    {
        //HmiLED_ON();
    }
}

void buttonPressed()
{
    button_pressed = true;
}

void buttonLongPress()
{
    button_long_pressed = true;
}

void buttonISR()
{
  // When button is being used through external interrupts, parameter INTERRUPT must be passed to read() function.
  button.read();
}

static void HmiOffTasks()
{
    //Serial.println("OFF");
    led_orange.setOffSingle();
    led_blue.setOffSingle();
}

static void HmiSmartModeOnTasks()
{
    //Serial.println("SM_ON");
    led_orange.setOffSingle();
    led_blue.setBreathSingle(HMI_LED_BREATH_TIME);
}

static void HmiSmartModeOffTasks()
{
    //Serial.println("SM_OFF");
    led_orange.setOffSingle();
    led_blue.setOnSingle();
}

static void HmiHeatingNowTasks()
{
    // Serial.println("HEATING NOW");
    // led_orange.setOnSingle();
    // led_blue.setOffSingle();
}

static void HmiOfflineTasks()
{
    //Serial.println("OFFLINE");
    led_orange.setBreathSingle(HMI_LED_BREATH_TIME_PARING);
    led_blue.setOffSingle();
}

static void HmiParingTasks()
{
    //Serial.println("PARING");
    led_orange.setOnSingle();
    led_blue.setOffSingle();
}

void HmiSetState(HMI_state_t state)
{
	hmi_state = state;
}

HMI_state_t HmiGetState(void)
{
	return hmi_state;
}

void HmiSetOrange()
{
    led_orange.setOnSingle();
}