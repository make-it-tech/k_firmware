#include "Energy.h"

//private
static void CseReceived(void);
static void CseSerialInput(void);
static void CseEverySecond(void);
static void EnergyUpdateLimit();
static void EnergyRead();

uint32_t energy_power_calibration   = HLW_PREF_PULSE;
uint32_t energy_voltage_calibration = HLW_UREF_PULSE;
uint32_t energy_current_calibration = HLW_IREF_PULSE;

unsigned long start = 0;

unsigned long start_limit_time = 0;
unsigned long last_energy_update = 0;

struct ENERGY {
  float voltage;             // 123.1 V
  float current;             // 123.123 A
  float active_power;        // 123.1 W
  float apparent_power;      // 123.1 VA
  float reactive_power;      // 123.1 VAr
  float power_factor;        // 0.12
  float frequency;           // 123.1 Hz
  float daily_sum;           // 123.123 kWh

  int32_t kWhtoday_delta;    // 1212312345 Wh 10^-5 (deca micro Watt hours) - Overflows to Energy.kWhtoday (HLW and CSE only)
  int32_t duW_telemetry_buffer;          //buffer to store energy until send it to MQTT
  int32_t duW_limit_buffer;

  uint8_t data_valid;

  bool power_on;
}Energy;

struct CSE {
  long voltage_cycle = 0;
  long current_cycle = 0;
  long power_cycle = 0;
  long power_cycle_first = 0;
  long cf_pulses = 0;
  long cf_pulses_last_time = CSE_PULSES_NOT_INITIALIZED;

  int byte_counter = 0;
  uint8_t *rx_buffer = nullptr;
  uint8_t power_invalid = 0;
  bool received = false;
} Cse;

//#related with limit cmmd
struct {
    time_t          start   = 0;    //epoch
    time_t          end     = 0;    //epoch
	  unsigned long   time_ms = 0;    
	  uint32_t        duW     = 0;
    bool            active  = false;
}limit;


static void CseReceived(void) 
{
	//Serial.println("CseReceived");
  //  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23
  // F2 5A 02 F7 60 00 03 61 00 40 10 05 72 40 51 A6 58 63 10 1B E1 7F 4D 4E - F2 = Power cycle exceeds range - takes too long - No load
  // 55 5A 02 F7 60 00 03 5A 00 40 10 04 8B 9F 51 A6 58 18 72 75 61 AC A1 30 - 55 = Ok, 61 = Power not valid (load below 5W)
  // 55 5A 02 F7 60 00 03 AB 00 40 10 02 60 5D 51 A6 58 03 E9 EF 71 0B 7A 36 - 55 = Ok, 71 = Ok
  // Hd Id VCal---- Voltage- ICal---- Current- PCal---- Power--- Ad CF--- Ck

  uint8_t header = Cse.rx_buffer[0];
  if ((header & 0xFC) == 0xFC) {
    AddLog(LOG_LEVEL_DEBUG, "CSE: Abnormal hardware");
    return;
  }

  // Get chip calibration data (coefficients) and use as initial defaults
  if (HLW_UREF_PULSE == energy_voltage_calibration) {
    long voltage_coefficient = 191200;  // uSec
    if (CSE_NOT_CALIBRATED != header) 
		{
      voltage_coefficient = Cse.rx_buffer[2] << 16 | Cse.rx_buffer[3] << 8 | Cse.rx_buffer[4];
    }
    energy_voltage_calibration = voltage_coefficient / CSE_UREF;
  }
  if (HLW_IREF_PULSE == energy_current_calibration) {
    long current_coefficient = 16140;  // uSec
    if (CSE_NOT_CALIBRATED != header) {
      current_coefficient = Cse.rx_buffer[8] << 16 | Cse.rx_buffer[9] << 8 | Cse.rx_buffer[10];
    }
    energy_current_calibration = current_coefficient;
  }
  if (HLW_PREF_PULSE == energy_power_calibration) {
    long power_coefficient = 5364000;  // uSec
    if (CSE_NOT_CALIBRATED != header) {
      power_coefficient = Cse.rx_buffer[14] << 16 | Cse.rx_buffer[15] << 8 | Cse.rx_buffer[16];
    }
    energy_power_calibration = power_coefficient / CSE_PREF;
  }

  uint8_t adjustement = Cse.rx_buffer[20];
  Cse.voltage_cycle = Cse.rx_buffer[5] << 16 | Cse.rx_buffer[6] << 8 | Cse.rx_buffer[7];
  Cse.current_cycle = Cse.rx_buffer[11] << 16 | Cse.rx_buffer[12] << 8 | Cse.rx_buffer[13];
  Cse.power_cycle = Cse.rx_buffer[17] << 16 | Cse.rx_buffer[18] << 8 | Cse.rx_buffer[19];
  Cse.cf_pulses = Cse.rx_buffer[21] << 8 | Cse.rx_buffer[22];


	//Energy.power_on = true;

  if (Energy.power_on) 
  {  // Powered on
    if (adjustement & 0x40) {  // Voltage valid
      Energy.voltage = (float)(energy_voltage_calibration * CSE_UREF) / (float)Cse.voltage_cycle;
    }
    if (adjustement & 0x10) {  // Power valid
      Cse.power_invalid = 0;
      if ((header & 0xF2) == 0xF2) {  // Power cycle exceeds range
        Energy.active_power = 0;
      } else {
        if (0 == Cse.power_cycle_first) { Cse.power_cycle_first = Cse.power_cycle; }  // Skip first incomplete Cse.power_cycle
        if (Cse.power_cycle_first != Cse.power_cycle) {
          Cse.power_cycle_first = -1;
          Energy.active_power = (float)(energy_power_calibration * CSE_PREF) / (float)Cse.power_cycle;
        } else {
          Energy.active_power = 0;
        }
      }
    } else {
      if (Cse.power_invalid < P_CSE7766_INVALID_POWER) {  // Allow measurements down to about 1W
        Cse.power_invalid++;
      } else {
        Cse.power_cycle_first = 0;
        Energy.active_power = 0;  // Powered on but no load
      }
    }
    if (adjustement & 0x20) {  // Current valid
      if (0 == Energy.active_power) {
        Energy.current = 0;
      } else {
        Energy.current = (float)energy_current_calibration / (float)Cse.current_cycle;
      }
    }
  } 
  else 
  {  // Powered off
    Cse.power_cycle_first = 0;
    Energy.voltage = 0;
    Energy.active_power = 0;
    Energy.current = 0;
  }
}

static void CseSerialInput(void) 
{
	//Serial.println("CseSerialInput");
  while (Serial2.available()) 
  {
    yield();
    uint8_t serial_in_byte = Serial2.read();

    if (Cse.received) 
		{
      Cse.rx_buffer[Cse.byte_counter++] = serial_in_byte;
      if (24 == Cse.byte_counter) 
			{
        //AddLogBuffer(LOG_LEVEL_DEBUG_MORE, Cse.rx_buffer, 24);

        uint8_t checksum = 0;
        for (uint32_t i = 2; i < 23; i++) 
				{ 
					checksum += Cse.rx_buffer[i]; 
				}
        if (checksum == Cse.rx_buffer[23]) 
				{
          Energy.data_valid = 0;
          CseReceived();
          Cse.received = false;
          return;
        } else 
				{
          do 
					{  // Sync buffer with data (issue #1907 and #3425)
            memmove(Cse.rx_buffer, Cse.rx_buffer +1, 24);
            Cse.byte_counter--;
          }while ((Cse.byte_counter > 2) && (0x5A != Cse.rx_buffer[1]));
          if (0x5A != Cse.rx_buffer[1]) 
					{
            AddLog(LOG_LEVEL_DEBUG, "CSE: D_CHECKSUM_FAILURE");
            Cse.received = false;
            Cse.byte_counter = 0;
          }
        }
      }
    } else 
		{
      if ((0x5A == serial_in_byte) && (1 == Cse.byte_counter)) 
			{// 0x5A - Packet header 2
        Cse.received = true;
      } else 
			{
        Cse.byte_counter = 0;
      }
      Cse.rx_buffer[Cse.byte_counter++] = serial_in_byte;
    }
  }
}

static void CseEverySecond(void) 
{
	if (Energy.data_valid > ENERGY_WATCHDOG) 
	{
		Cse.voltage_cycle = 0;
		Cse.current_cycle = 0;
		Cse.power_cycle = 0;
	} 
	else 
	{
		if(CSE_PULSES_NOT_INITIALIZED == Cse.cf_pulses_last_time) 
		{
			Cse.cf_pulses_last_time = Cse.cf_pulses;  // Init after restart
		} 
		else 
		{
      
			uint32_t cf_pulses = 0;
			if (Cse.cf_pulses < Cse.cf_pulses_last_time) 
			{  // Rolled over after 0xFFFF (65535) pulses
				cf_pulses = (0x10000 - Cse.cf_pulses_last_time) + Cse.cf_pulses;
			} 
			else 
			{
				cf_pulses = Cse.cf_pulses - Cse.cf_pulses_last_time;
			}
			if (cf_pulses && Energy.active_power)  
			{
				uint32_t delta = (cf_pulses * energy_power_calibration) / 36;
				// prevent invalid load delta steps even checksum is valid (issue #5789):
				// prevent invalid load delta steps even checksum is valid but allow up to 4kW (issue #7155):
		//        if (delta <= (4000 * 1000 / 36)) {  // max load for S31/Pow R2: 4.00kW
				// prevent invalid load delta steps even checksum is valid but allow up to 5.5kW (issue #14156):
				if (delta <= (5500 * 1000 / 36)) 
				{  // max load for Pow R3: 5.50kW
					Cse.cf_pulses_last_time = Cse.cf_pulses;
					Energy.kWhtoday_delta += delta;
				}
				else 
				{
					AddLog(LOG_LEVEL_DEBUG, "CSE: Overload");
					Cse.cf_pulses_last_time = CSE_PULSES_NOT_INITIALIZED;
				}
				//EnergyUpdateToday();
        Energy.duW_telemetry_buffer = Energy.duW_telemetry_buffer   + delta; 
        Energy.duW_limit_buffer     = Energy.duW_limit_buffer       + delta;
        // DEBUG_PRINT("delta: ");
        // DEBUG_PRINT_LN(delta);
			}
		}
	}
}

void AddLog(uint8_t loglevel, char log_data[180])
{
    if (loglevel <= LOG_LEVEL_INFO)
    {
        Serial.printf("%s\r\n", log_data);
    }
}

void EnergyInit(void) 
{
  //Selector
	pinMode(26, OUTPUT);
  digitalWrite(26, 1);
  
  //Relay~
  pinMode(RELAY_PIN, OUTPUT);
  //EnergyPowerON();

  Cse.rx_buffer = (uint8_t*)(malloc(CSE_BUFFER_SIZE));
	Serial2.begin(4800, SERIAL_8N1, 16, 17);
	CseSerialInput();
}

void EnergyTasks()
{
  
  if(WifiGetState() != WIFI_STATE_ONLINE)
  {
      #ifdef RELAY_CLOSE_ON_OFFLINE
        EnergyPowerON();
      #endif
  }

  if(!is_KluActive())
  {
      //DEBUG_PRINT("KLU is not active - reading energy now");
      //get access to opto coupler with GPIO 26 at HIGH
      digitalWrite(26, 1);
      EnergyRead();
  }
  else
  {
    if(!is_KluReadingMsg())
      {
          EnergyRead();
      }
  }
}

void EnergyPowerON()
{
    DEBUG_PRINT_LN("Energy:  EnergyPowerON()");
    digitalWrite(RELAY_PIN, HIGH);
    Energy.power_on = true;
    MqttSendRelayState();
    EnergyLimitCancel(); 
}

void EnergyPowerOFF()
{
    DEBUG_PRINT_LN("Energy:  EnergyPowerOFF()");
    digitalWrite(RELAY_PIN, LOW);
    Energy.power_on = false;
    MqttSendRelayState();
    EnergyLimitCancel();   
    
}

float EnergyGetLastSlot()
{
    // DEBUG_PRINT("GetSlot Wh_slot buffer: ");
    // DEBUG_PRINT_LN(Energy.duW_telemetry_buffer);

    float Wh_slot = float(Energy.duW_telemetry_buffer) / 100000.0;
    Energy.duW_telemetry_buffer = 0;

    // DEBUG_PRINT("GetSlot Wh_slot float: ");
    // DEBUG_PRINT_LN(float(Wh_slot));
    return Wh_slot;
}

uint16_t EnergyGetVoltage()
{
    return Energy.voltage;
}

float EnergyGetCurrent()
{
    return Energy.current;
}

uint16_t EnergyGetPower()
{
    return Energy.active_power;
}

void EnergyLimit(float limit_Wh, unsigned long limit_time_s)
{
    EnergyPowerON();
    DEBUG_PRINT("[HLW] New limit Wh: ");
    DEBUG_PRINT(limit_Wh);
    DEBUG_PRINT(" time_s: ");
    DEBUG_PRINT_LN(limit_time_s);

    //reset buffer
    Energy.duW_limit_buffer = 0;

    limit.active   = true;
    limit.duW      = static_cast<uint32_t>(limit_Wh * float(100000.0));
    // DEBUG_PRINT("[HLW] New limit duW in float : ");
    // DEBUG_PRINT(limit_Wh * 100000.0);
    // DEBUG_PRINT(" [HLW] New limit duW in int : ");
    // DEBUG_PRINT_LN(limit.duW);
    limit.end      = WifiGetEpoch() + limit_time_s;
    limit.time_ms  = limit_time_s; //duration in ms
    start_limit_time    = millis();
    
}

static void EnergyUpdateLimit()
{
    if(millis() - last_energy_update > 1000 && limit.active)
    {   
        unsigned long now = WifiGetEpoch();
        // DEBUG_PRINT("HWL_Update_energy: epoch time: ");
        // DEBUG_PRINT_LN(hlw_now);
        
        if (Energy.duW_limit_buffer >= limit.duW)
        {
            //limit reached -> open the relay
            EnergyPowerOFF();
            limit.active = false;
            limit.time_ms = 0;
            limit.duW = 0;
            //limit_recovery = false;
        }
        if( now > limit.end)
        {
            //time limit reached -> open the relay
            EnergyPowerOFF();
            limit.active = false;
            limit.time_ms = 0;
            limit.duW = 0;
            //limit_recovery = false;
        }
        last_energy_update = millis();
    }
}

void EnergyLimitCancel()
{
    limit.active = false;
}

bool is_EnergyRelayOn()
{
  return Energy.power_on;
}

static void EnergyRead()
{
    CseSerialInput();
    unsigned long now = millis();
    if(now-start > 1000)
    {
      //DEBUG_PRINT_LN("CseEverySecond");
      start = now;
      CseEverySecond();
    }
    EnergyUpdateLimit();
}


