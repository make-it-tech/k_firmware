#include "ds18b20.h"

//Private prototypes
static bool temperatureIsValid(float temperaure);
static void dsGetSample();

const int oneWireBus = DS18B20_PIN;
OneWire  one(oneWireBus);  // on pin 10
byte rom[8];
byte resp[8];
static float temperature = 0.0;
static unsigned long last_update = 0;

void dsInit()
{

}

void dsTasks()
{
    unsigned long now = millis();
    if(now-last_update > 1000)
    {
        dsGetSample();
        last_update = now;
    }
}

static void dsGetSample()
{
    float temp = 0.0;
    // Get the ROM address
    one.reset();
    one.write(0x33);
    one.read_bytes(rom, 8);
    // Get the temp
    one.reset();
    one.write(0x55);
    one.write_bytes(rom,8);
    one.write(0x44);
    delay(10);
    one.reset();
    one.write(0x55);
    one.write_bytes(rom, 8);
    one.write(0xBE);
    one.read_bytes(resp, 9);

    byte MSB = resp[1];
    byte LSB = resp[0];

    int16_t intTemp = ((MSB << 8) | LSB); //using two's compliment 16-bit
    temp = ((double)intTemp)/16.0;
    if (temperatureIsValid(temp))
    {
        temperature = temp;
    }
}

float DsGetTemperature()
{
    return temperature;
}

static bool temperatureIsValid(float temperature)
{
    //Serial.println(temperature);
    if(temperature > MIN_TEMPERATURE  && temperature < MAX_TEMPERATURE)
    {
        //Serial.println("is valid");
        return true;
    }
    return false;
}


