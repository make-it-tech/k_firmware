#ifndef DS_H
#define DS_H

//#include <OneWire.h>
#include "../lib/OneWire/OneWire.h"

#define DS18B20_PIN         4
#define MIN_TEMPERATURE     1.0
#define MAX_TEMPERATURE     100.0

void dsInit();
void dsTasks();
float DsGetTemperature();


#endif