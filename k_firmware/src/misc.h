#ifndef MISC_H
#define MISC_H

#include "klugit_config.h"

void MiscReset();
void MiscRestart();

#endif