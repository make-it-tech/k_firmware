#ifndef OTA_H
#define OTA_H

#include "klugit_config.h"

#ifdef ESP32
    #include "esp32_ota.h"
#endif

void OtaInit();
void OtaTasks();
void OtaUpdate(const char* add);

#endif