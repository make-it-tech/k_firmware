#include "sensors.h"

float pipe_temperature  = 0.0;
float hot_temperature   = 0.0;
float water_volume      = 0.0;
unsigned long last_update = 0;

void SensorsInit()
{
    //DS
    dsInit();
}
void SensorsTasks()
{
    //tasks
    dsTasks();
    unsigned long now = millis();
    if(now-last_update > 1000)
    {
        pipe_temperature = DsGetTemperature();
        hot_temperature  = KluGetTemperature();
        
        last_update = now;
    }
}

float SensorsGetPipeTemp()
{
    return pipe_temperature;
}
float SensorsGetHotTemp()
{
    return hot_temperature;
}
float SensorsGetWaterVolume()
{
    water_volume     = KluGetWaterVolume();
    return water_volume;
}
