#ifndef ENERGY_H
#define ENERGY_H

#include "klugit_config.h"
#include <inttypes.h>
#include <Stream.h>
#include <Arduino.h>
#include "kwifi.h"
#include "klu.h"

#define RELAY_PIN               		32

#define ENERGY_MAX_PHASES 1
#define ENERGY_WATCHDOG        4        // Allow up to 4 seconds before deciding no valid data present

#define CSE_MAX_INVALID_POWER       128        // Number of invalid power receipts before deciding active power is zero

#define CSE_NOT_CALIBRATED          0xAA

#define CSE_PULSES_NOT_INITIALIZED  -1

#define CSE_PREF                    1000
#define CSE_UREF                    1000

#define CSE_BUFFER_SIZE             25

#define P_CSE7766_INVALID_POWER 128  // SetOption39 - (CSE7766) Number of invalid power measurements before declaring it invalid allowing low load measurments (default 128)

//from tasmota.h
const uint32_t HLW_PREF_PULSE = 12530;      // was 4975us = 201Hz = 1000W
const uint32_t HLW_UREF_PULSE = 1950;       // was 1666us = 600Hz = 220V
const uint32_t HLW_IREF_PULSE = 3500;       // was 1666us = 600Hz = 4.545A

void EnergyInit(void);
void EnergyTasks();
void EnergyPowerON();
void EnergyPowerOFF();
void AddLog(uint8_t loglevel, char log_data[180]);
float EnergyGetLastSlot();
uint16_t EnergyGetVoltage();
float EnergyGetCurrent();
uint16_t EnergyGetPower();
void EnergyLimit(float limit_Wh, unsigned long limit_time_s);
void EnergyLimitCancel();
bool is_EnergyRelayOn();

enum LoggingLevels {LOG_LEVEL_NONE, LOG_LEVEL_ERROR, LOG_LEVEL_INFO, LOG_LEVEL_DEBUG, LOG_LEVEL_DEBUG_MORE};




#endif