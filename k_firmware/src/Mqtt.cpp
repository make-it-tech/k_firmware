#include "Mqtt.h"

//PRIVATE PROTOTYPES

static void Mqtt_Received_Callback(char* topic, byte* payload, unsigned int length);
static void Mqtt_Standby_Tasks();
static void Mqtt_Send_Telemetry_Tasks();
static void Mqtt_Send_Stat_Tasks();
static void Mqtt_SetState(Mqtt_state_t state);
static void MqttSendWakeupMsg();
static void MqttBuildTopics();

//Global objects
//BearSSL::WiFiClientSecure mqtt_client;
WiFiClient espClient;
PubSubClient mqtt_client(espClient);

//Global variables
Mqtt_event_t Mqtt_event;
Mqtt_state_t Mqtt_state;
Machine_state_t Machine_state = MACHINE_STATE_INIT;

//Char ID
String ID = WiFi.macAddress();
char char_ID[7];

// MQTT connection parameters
byte willQoS = 0;
const char* willMessage = MQTT_WILL_MSG;
boolean willRetain = false;
uint8_t mqtt_connections_attemps = 0;
time_t nowd; 
unsigned long last_telemetry_msg = MQTT_TELEMETRY_SAMPLING*2;       //value to save elapsed time
unsigned long last_stat_msg      = MQTT_STAT_SAMPLING;       //value to save elapsed time
unsigned long last_connect        = 1000;
unsigned long last_vibration_msg = 0;       //value to save elapsed time

StaticJsonDocument<MQTT_JSON_PAYLOAD_SIZE> mqtt_payload;
char carray_mqtt_payload[MQTT_ARRAY_OUTPUT_SIZE];

//Build topics
char mqtt_topic_tele_sensor[MQTT_TOPIC_SIZE];
char mqtt_topic_cmnd_sub[MQTT_TOPIC_SIZE];
char mqtt_topic_server_sub[MQTT_TOPIC_SIZE];
char mqtt_topic_stat_stat[MQTT_TOPIC_SIZE];
char mqtt_topic_stat_birth[MQTT_TOPIC_SIZE];
char mqtt_topic_tele_vibration[MQTT_TOPIC_SIZE];
char mqtt_topic_will[MQTT_TOPIC_SIZE];
char mqtt_topic_stat_warning[MQTT_TOPIC_SIZE];
char mqtt_topic_cmnd_limit[MQTT_TOPIC_SIZE];
char mqtt_topic_stat_info[MQTT_TOPIC_SIZE];
char mqtt_topic[MQTT_TOPIC_SIZE];

char cmnd_c_array[]             = "cmnd";
char stat_c_array[]             = "stat";
char ON_c_array[]               = "ON";
char OFF_c_array[]              = "OFF";
char backoffice_reset[]         = "reset";
char backoffice_erase[]         = "erase";
char backoffice_restart[]       = "restart";
char backoffice_i2c_recovery[]  = "i2c_recovery";
char server_smartmode_on[]      = "SMARTMODE_ON";
char server_smartmode_off[]     = "SMARTMODE_OFF";
char server_off[]               = "OFF";
char server_heatnow[]           = "HEATNOW_ON";
char servers[]                  = "server";
char stats[]                     = "STAT";
char warning[]                  = "WARNING";
char limit[]                    = "LIMIT";

static bool inited = false;

// PUBLIC FUNCTIONS
void MqttInit()
{
    //Remove MAC separators ":" 
    String ID = WiFi.macAddress();
    for (size_t i = 1; i < 11; i++)
    {
      ID.remove(i*2,1);
    }  
    ID = ID.substring(6);
    ID.toCharArray(char_ID, 7);

    MqttBuildTopics();

    mqtt_client.setServer(MQTT_HOST, MQTT_PORT);
    mqtt_client.setCallback(Mqtt_Received_Callback);
    Mqtt_Connect();

    //send wakeup message
    MqttSendWakeupMsg();

    Mqtt_SetState(MQTT_STATE_STANDBY);
    inited = false;

}

void MqttTasks()
{
  if(!inited)
  {
    MqttInit();
    inited = true;
  }

  switch (Mqtt_state)
  {
  case MQTT_STATE_STANDBY:
    Mqtt_Standby_Tasks();
    break;

  case MQTT_STATE_SEND_TELEMETRY:
    Mqtt_Send_Telemetry_Tasks();
    break;

  case MQTT_STATE_SEND_STAT:
    Mqtt_Send_Stat_Tasks();
    break; 

  default:
    //do something;
    break;
  }
  
  if (Mqtt_event != MQTT_EVENT_NONE)
  {
    Mqtt_event = MQTT_EVENT_NONE;
  }
  mqtt_client.loop();
  
}

void MqttSendRelayState()
{
  //Build topic
  strcpy(mqtt_topic, MQTT_PREFIX_STAT);
  strcat(mqtt_topic, MQTT_GROUP);
  strcat(mqtt_topic, "/");
  strcat(mqtt_topic, char_ID);
  strcat(mqtt_topic, MQTT_TYPE_RESULT);

  const char *payload = "";
  mqtt_payload.clear();

  if (is_EnergyRelayOn())
  {
    payload = "ON";
  }
  else
  {
    payload = "OFF";
  }  
  mqtt_payload["POWER"] = payload;
  carray_mqtt_payload[0] = 0;
  serializeJson(mqtt_payload, carray_mqtt_payload);
  mqtt_client.publish(mqtt_topic, carray_mqtt_payload, true);  

  //Mqtt_event = MQTT_EVENT_STAT_TIMEOUT;
}

void MqttSendInfo(uint8_t error_type)
{
  //Build topic
  strcpy(mqtt_topic_stat_info, MQTT_PREFIX_STAT);
  strcat(mqtt_topic_stat_info, MQTT_GROUP);
  strcat(mqtt_topic_stat_info, "/");
  strcat(mqtt_topic_stat_info, char_ID);
  strcat(mqtt_topic_stat_info, MQTT_TYPE_INFO);

  mqtt_payload.clear();   
  mqtt_payload["INFO"] = error_type;
  serializeJson(mqtt_payload, carray_mqtt_payload);
  mqtt_client.publish(mqtt_topic_stat_info, carray_mqtt_payload, false); 
}

void MqttSendButtonEvent(bool longpress)
{
  //Build topic
  strcpy(mqtt_topic, MQTT_PREFIX_CMND);
  strcat(mqtt_topic, MQTT_GROUP);
  strcat(mqtt_topic, "/");
  strcat(mqtt_topic, char_ID);
  strcat(mqtt_topic, "/BUTTON");
  const char *payload = "";
  if(longpress)
  {
    //send info 
  }
  else
  {
    switch (Machine_state)
    {
    case MACHINE_STATE_OFF:
      payload = "{\"button\":\"button_click_on\"}";
      break;
    
    case MACHINE_STATE_SMART_MODE_ON:
      payload = "{\"button\":\"button_click_smartmode\"}";
      break;

    case MACHINE_STATE_SMART_MODE_OFF:
      payload = "{\"button\":\"button_click_off\"}";
    
    default:
      break;
    }
    mqtt_client.publish(mqtt_topic, payload, false);
  }
}

Mqtt_state_t Mqtt_GetState(void)
{
	return Mqtt_state;
}

// PRIVATE FUNCTIONS
static void Mqtt_Standby_Tasks()
{
  unsigned long now = millis();
  static bool first_time = true;

  if (now - last_connect > 1000) 
  {
    Mqtt_Connect();
    last_connect = now;
  }

  if (now - last_stat_msg > MQTT_STAT_SAMPLING) 
  {
    
    Mqtt_event = MQTT_EVENT_STAT_TIMEOUT;
    last_stat_msg = now;
  }

  else if (now - last_telemetry_msg > MQTT_TELEMETRY_SAMPLING) 
  {
    if(!first_time)
    {
        Mqtt_event = MQTT_EVENT_TELEMETRY_TIMEOUT;
        last_telemetry_msg = millis();
    }
    else
    {
      first_time = false;
      last_telemetry_msg = millis();
    }
    
    
  }

  if (Mqtt_event == MQTT_EVENT_TELEMETRY_TIMEOUT)
  {
    Mqtt_SetState(MQTT_STATE_SEND_TELEMETRY);
  }
  else if (Mqtt_event == MQTT_EVENT_STAT_TIMEOUT)
  {
    Mqtt_SetState(MQTT_STATE_SEND_STAT);
  }
  
}

static void Mqtt_Send_Telemetry_Tasks()
{ 
  //DEBUG_PRINT_LN("Send Telemetry");
  static float temp_hot = 0.0;
  static float temp_pipe = 0.0;
  static float water_vol = 0.0;

  temp_pipe = SensorsGetPipeTemp();

  //Create Json object
  //DynamicJsonDocument telemetry(MQTT_TELEMETRY_JSON_SIZE);
  mqtt_payload.clear();
  mqtt_payload["Time"] = WifiGetEpoch();

  JsonObject ENERGY = mqtt_payload.createNestedObject("ENERGY");
  ENERGY["Voltage"] = EnergyGetVoltage(); //Voltage;
  ENERGY["Power"]   = EnergyGetPower();
  ENERGY["Wh"]      = serialized(String(EnergyGetLastSlot(),3));

  JsonObject Temp = mqtt_payload.createNestedObject("Temp");
  Temp["Pipe"] = serialized(String(temp_pipe,2));

  if(is_KluActive())
  {
    JsonObject Klu = mqtt_payload.createNestedObject("KLU");
    temp_hot = SensorsGetHotTemp();
    water_vol = SensorsGetWaterVolume();
    Klu["Hot"] = serialized(String(temp_hot,2));
    Klu["Vol"] = serialized(String(water_vol,2));
  }
  
  //serialize payload
  carray_mqtt_payload[0] = 0;
  serializeJson(mqtt_payload, carray_mqtt_payload);
  
  //Send or save the telemetry
  if(WifiGetState() == WIFI_STATE_ONLINE)
  {
    if(!mqtt_client.connected())
      {
        // Reconnect to broker, if not connected
        Mqtt_Connect();
      }
      mqtt_client.publish(mqtt_topic_tele_sensor, carray_mqtt_payload, true);
      DEBUG_PRINT("Sending telemetry: ");
      DEBUG_PRINT_LN(carray_mqtt_payload);  
  }
  else if (WifiGetState() == WIFI_STATE_OFFLINE)
  {
    //save telemetry to flash~
    DEBUG_PRINT("Saving telemetry: ");
    DEBUG_PRINT_LN(carray_mqtt_payload);

    //todo: remove this
    if(!mqtt_client.connected())
      {
        // Reconnect to broker, if not connected
        Mqtt_Connect();
      }
      mqtt_client.publish(mqtt_topic_tele_sensor, carray_mqtt_payload, true);
  }
  
  Mqtt_event = MQTT_EVENT_TELEMETRY_SENT;
  if(Mqtt_event == MQTT_EVENT_TELEMETRY_SENT)
  {
    Mqtt_SetState(MQTT_STATE_STANDBY);
  }
}

static void Mqtt_Send_Stat_Tasks()
{
  //DEBUG_PRINT_LN("Mqtt_Read_Stat_Tasks");

  mqtt_payload.clear();
  mqtt_payload["Time"] = WifiGetEpoch();
  if (is_EnergyRelayOn())
  {
    // Plug is ON
    mqtt_payload["POWER"] = "ON";
  }
  else
  {
    mqtt_payload["POWER"] = "OFF";
  }
  JsonObject Wifi = mqtt_payload.createNestedObject("Wifi");
  Wifi["RSSI"] = WiFi.RSSI();

  carray_mqtt_payload[0] = 0;
  serializeJson(mqtt_payload, carray_mqtt_payload);  

  if (!mqtt_client.connected())
  {
    // Reconnect to broker, if not connected
    Mqtt_Connect();
  }
  mqtt_client.publish(mqtt_topic_stat_stat, carray_mqtt_payload, true);

  Mqtt_event = MQTT_EVENT_STAT_SENT;

  if(Mqtt_event == MQTT_EVENT_STAT_SENT)
  {
    Mqtt_SetState(MQTT_STATE_STANDBY);
  }

}

bool Mqtt_Connect()
{
    if(WifiGetState() == WIFI_STATE_OFFLINE || WifiGetState() == WIFI_STATE_DISCONNECTED)
    {
      mqtt_client.disconnect();
      return false;
    }

    if(mqtt_client.connected())
    {
      return true;
    }

    DEBUG_PRINT("Attempting MQTT connection... ");
    
    //WILL TOPIC
    strcpy(mqtt_topic_will, MQTT_PREFIX_TELE);
    strcat(mqtt_topic_will, MQTT_GROUP);
    strcat(mqtt_topic_will,"/");
    strcat(mqtt_topic_will, char_ID);
    strcat(mqtt_topic_will,"/LWT");

    // Attempt to connect
    if (mqtt_client.connect(char_ID, "usensores", "ctrlc#001100", mqtt_topic_will, willQoS, willRetain, willMessage, true)) 
    {
      DEBUG_PRINT_LN("connected");
      mqtt_client.publish(mqtt_topic_will, "Online", true);
      
      //Subscribe to topics      
      mqtt_client.subscribe(mqtt_topic_cmnd_sub); 
      mqtt_client.subscribe(mqtt_topic_server_sub);
      mqtt_connections_attemps = 0;
      return true;
    } 
    else 
    {
      mqtt_connections_attemps++;
      DEBUG_PRINT("failed, rc=");
      DEBUG_PRINT_LN(mqtt_client.state());
      return false;
    }
  }

static void Mqtt_Received_Callback(char* mqtt_topic, byte* mqtt_payload, unsigned int length) 
{  
  DEBUG_PRINT("topic: ");
  DEBUG_PRINT_LN(mqtt_topic);
  DEBUG_PRINT("payload: ");
  
  //break topic
  char* prefix  = strtok(mqtt_topic, "/");
  char* device  = strtok(NULL, "/");
  char* id      = strtok(NULL, "/");
  char* type    = strtok(NULL, "/");

  String type_s = type;
  String device_s = device;

  //Payload to str
  String payload = "";  
  for (unsigned int i = 0; i < length; i++) 
  {
    DEBUG_PRINT((char)mqtt_payload[i]);
    payload.concat((char)mqtt_payload[i]);
  }
  DEBUG_PRINT_LN();
  if(strcmp(prefix,cmnd_c_array)==0)
  {
    //CMND
    if(type_s == "POWER")
    {  
      // POWER
      if (payload == "ON")
      {
        //send event to turn device on
        //DEBUG_PRINT_LN("Turning ON");
        EnergyPowerON();
      }
      else if (payload == "OFF")
      {
        //DEBUG_PRINT_LN("Turning OFF");
        EnergyPowerOFF();
      }
    }   

    else if(type_s == "UPDATE")
    {
      //UPDATE
      const size_t capacity = MQTT_OTA_UPDATE_BUFFER;
      DynamicJsonDocument doc(capacity);    
      deserializeJson(doc, payload);
      const char* add = doc["add"]; 

      DEBUG_PRINT("Going to update by mqtt command");
      MqttSendInfo(INFO_OTA_UPDATE);
      OtaUpdate(add);
    }

    else if(type_s == "BACKOFFICE")
    {
      //BACKOFFICE
      const size_t capacity = MQTT_OTA_UPDATE_BUFFER;
      DynamicJsonDocument doc(capacity);    
      deserializeJson(doc, payload);
      const char* cmnd = doc["cmnd"]; 

      if (strcmp(cmnd, backoffice_reset) == 0)
      {
        MqttSendInfo(INFO_BACKOFFICE_RESET);
        MiscReset();
        
      }
      else if (strcmp(cmnd, backoffice_erase) == 0)
      {
        MqttSendInfo(INFO_BACKOFFICE_ERASE);
        //Wifi_Erase();
      }
      else if (strcmp(cmnd,backoffice_restart) == 0)
      {
        MqttSendInfo(INFO_BACKOFFICE_RESTART);
        MiscRestart();
      }
      
    }
    else if (type_s == "LIMIT")
    {
      const size_t capacity = 256;
      DynamicJsonDocument doc(capacity);    
      deserializeJson(doc, payload);
      unsigned long limit_time_s = uint(doc["duration"]);
      deserializeJson(doc, payload);
      unsigned long limit_ws     = uint(doc["limit"]); 
      // * 3600;
      
      DEBUG_PRINT("limit in Wh: ");
      DEBUG_PRINT_LN(limit_ws);

      EnergyLimit(limit_ws, limit_time_s);
    }
  }  

  if (strcmp(prefix, stat_c_array) == 0)
  {
    //stat
    if ((device_s == "server") && (type_s == "STAT"))
    {
      const size_t capacity = MQTT_OTA_UPDATE_BUFFER;
      DynamicJsonDocument doc(capacity);    
      deserializeJson(doc, payload);
      const char* state = doc["state"];
      if(strcmp(state, server_smartmode_on) == 0)
      {
        Machine_state = MACHINE_STATE_SMART_MODE_ON;
        HmiSetState(HMI_STATE_SM_ON);
      }
      else if (strcmp(state, server_smartmode_off) == 0)
      {
        Machine_state = MACHINE_STATE_SMART_MODE_OFF;
        HmiSetState(HMI_STATE_SM_OFF);
      }
      else if (strcmp(state, server_off) == 0)
      {
        Machine_state = MACHINE_STATE_OFF;
        HmiSetState(HMI_STATE_OFF);
      }
      else if (strcmp(state, server_heatnow) == 0)
      {
        Machine_state = MACHINE_STATE_HEAT_NOW_ON;
        HmiSetState(HMI_STATE_HEATING_NOW);
      }
      else
      {
        //heatnow
        //HMI_ServerState(2);
      }
    }
  }
}

void MqttReconnect()
{
  DEBUG_PRINT_LN("Mqtt: MqttReconnect()");
  mqtt_client.disconnect();
  Mqtt_Connect();
}

static void Mqtt_SetState(Mqtt_state_t state)
{
	Mqtt_state = state;
}

static void MqttBuildTopics()
{
  //Create MQTT topic tele/klunit/ID/SENSOR 
  strcpy(mqtt_topic_tele_sensor, MQTT_PREFIX_TELE);
  strcat(mqtt_topic_tele_sensor, MQTT_GROUP);
  strcat(mqtt_topic_tele_sensor, "/");
  strcat(mqtt_topic_tele_sensor, char_ID);
  strcat(mqtt_topic_tele_sensor, MQTT_TYPE_SENSOR);

  strcpy(mqtt_topic_cmnd_sub, MQTT_PREFIX_CMND);
  strcat(mqtt_topic_cmnd_sub, MQTT_GROUP);
  strcat(mqtt_topic_cmnd_sub, "/");
  strcat(mqtt_topic_cmnd_sub, char_ID);
  strcat(mqtt_topic_cmnd_sub, "/#");
 
  strcpy(mqtt_topic_stat_stat, MQTT_PREFIX_STAT);
  strcat(mqtt_topic_stat_stat, MQTT_GROUP);
  strcat(mqtt_topic_stat_stat, "/");
  strcat(mqtt_topic_stat_stat, char_ID);
  strcat(mqtt_topic_stat_stat, MQTT_TYPE_STAT);
  
  // mqtt topic stat birth
  strcpy(mqtt_topic_stat_birth, MQTT_PREFIX_STAT);
  strcat(mqtt_topic_stat_birth, MQTT_GROUP);
  strcat(mqtt_topic_stat_birth, "/");
  strcat(mqtt_topic_stat_birth, char_ID);
  strcat(mqtt_topic_stat_birth, MQTT_TYPE_BIRTH);

  strcpy(mqtt_topic_tele_vibration, MQTT_PREFIX_TELE);
  strcat(mqtt_topic_tele_vibration, MQTT_GROUP);
  strcat(mqtt_topic_tele_vibration, "/");
  strcat(mqtt_topic_tele_vibration, char_ID);
  strcat(mqtt_topic_tele_vibration, MQTT_TYPE_VIBRATION);

  //
  strcpy(mqtt_topic_server_sub, MQTT_PREFIX_STAT);
  strcat(mqtt_topic_server_sub, "/server");
  strcat(mqtt_topic_server_sub, "/");
  strcat(mqtt_topic_server_sub, char_ID);
  strcat(mqtt_topic_server_sub, MQTT_TYPE_STAT);

  strcpy(mqtt_topic_stat_warning, MQTT_PREFIX_STAT);
  strcat(mqtt_topic_stat_warning, MQTT_GROUP);
  strcat(mqtt_topic_stat_warning, "/");
  strcat(mqtt_topic_stat_warning, char_ID);
  strcat(mqtt_topic_stat_warning, MQTT_TYPE_WARNING);

  //LIMIT
  strcpy(mqtt_topic_cmnd_limit, MQTT_PREFIX_CMND);
  strcat(mqtt_topic_cmnd_limit, MQTT_GROUP);
  strcat(mqtt_topic_cmnd_limit, "/");
  strcat(mqtt_topic_cmnd_limit, char_ID);
  strcat(mqtt_topic_cmnd_limit, MQTT_TYPE_LIMIT);
}

static void MqttSendWakeupMsg()
{
  mqtt_payload.clear();   
  mqtt_payload["TIME"] = WifiGetEpoch();
  if(WifiWakeFromFactoryReset())
  {
    // unit started for the first time
    // or started after a factory reset
    mqtt_payload["MSG"]  = "birth";
    
    //HMI_RegisterBirth();
  }  
  else
  {   
    mqtt_payload["MSG"]  = "start";  
  }

  mqtt_payload["FW_VERSION"] = serialized(String(FW_VERSION));  
  mqtt_payload["CONN_TIME"]  = WifiConnectionTime();
  serializeJson(mqtt_payload, carray_mqtt_payload);
  mqtt_client.publish(mqtt_topic_stat_birth, carray_mqtt_payload, true);
}

void Mqtt_Signal_Info()
{

}