#ifndef K_WIFI_H
#define K_WIFI_H

#include "klugit_config.h"
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <Arduino.h>
#include <ESP32Ping.h>
#include "hmi.h"
#ifdef ESP32
    #include <WiFi.h>
    #include "..\lib\WiFiManager-master\WiFiManager-master\WiFiManager.h"
#else
     #include <ESP8266WiFi.h>
     #include "../lib/WiFiManager-master/WiFiManager.h"         // https://github.com/tzapu/WiFiManager
#endif

#define WIFI_CONNECTION_ATTEMPS_S   60*5            
#define WIFI_AP_PORTAL_TIMEOUT_S    5*60    //if not configured in 30 minutes, start the program
#define WIFI_AP_SCAN_MIN_SIGNAL     20
#define WIFI_DISCONNECTED_TIMEOUT_S 5*60    //if no connection during 60s
#define WIFI_OFFLINE_TIMEOUT_S      30*60
#define WIFI_STA_CON_TIMEOUT_S      60*5

//define CREDENTIALS to use SSID and pwd intead of wifimanager
//#define CREDENTIALS

#define WIFI_SSID  "Klugit Energy AP"
#define WIFI_PSWD  ""
typedef enum
{
	/** @brief No event */
	WIFI_EVENT_NONE = 0,
	WIFI_EVENT_CONNECTED,
	WIFI_EVENT_DISCONNECTED,
    WIFI_EVENT_PING,
    WIFI_EVENT_NO_PING,
    WIFI_EVENT_FACTORY_RESET,
    WIFI_EVENT_PARED
}wifi_event_t;

typedef enum {
	WIFI_STATE_CONNECTED = 0,
	WIFI_STATE_DISCONNECTED,
    WIFI_STATE_ONLINE,
    WIFI_STATE_OFFLINE,
    WIFI_STATE_PARING
}wifi_state_t;  



//PUBLIC
void WifiInit();
void WifiTasks();

unsigned long WifiGetEpoch();
wifi_state_t WifiGetState(void);

void WifiEraseCredentials();
bool WifiWakeFromFactoryReset();
unsigned long WifiConnectionTime();
bool WifiPingServer();

#endif 