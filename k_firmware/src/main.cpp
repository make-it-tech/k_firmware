#ifdef CORE_DEBUG_LEVEL
#undef CORE_DEBUG_LEVEL
#endif

#define CORE_DEBUG_LEVEL 3
#define LOG_LOCAL_LEVEL ESP_LOG_DEBUG

#include <Arduino.h>
#include "kwifi.h"
#include "Mqtt.h"
#include "Energy.h"
//#include "HMI.h"
#include "sensors.h"
#include "klu.h"



void setup() 
{
  Serial.begin(115200);
  HmiInit();
  EnergyInit();
  WifiInit();

  SensorsInit();    
  
  MqttInit();

  if(is_KluActive())
  {
    KluInit();
  }
  delay(1000);
}

void loop()
{
  SensorsTasks();
  WifiTasks();
  MqttTasks();
  HmiTasks();
  EnergyTasks();

  if(is_KluActive())
  {
    KluTasks();
  } 
  
}