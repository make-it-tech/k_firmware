#include "esp32_ota.h"

esp32FOTA esp32("esp32-fota-http", 1, false);

void esp32_OtaUpdate(const char* add)
{
    String address = String(add);
    esp32.forceUpdate(address, false);
}