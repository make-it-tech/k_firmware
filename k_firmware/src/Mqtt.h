#ifndef MQTT_H
#define MQTT_H

#include <PubSubClient.h>
#include <WiFiClientSecure.h>
#include <time.h>
#include <ArduinoJson.h>
#include "ota.h"
#include "klugit_config.h"
#include "kwifi.h"
#include "sensors.h"
#include "Energy.h"
#include "misc.h"

#include "hmi.h"


#define SECRET
#define MQTT_AWS_BROKER

#define MQTT_WILL_MSG				"Offline"
#define MQTT_DEFAULT_JSON_SIZE		128
#define MQTT_TELEMETRY_JSON_SIZE 	128
#define MQTT_STAT_JSON_SIZE	 	 	128
#define MQTT_OTA_UPDATE_BUFFER 		100
#define MQTT_SENSOR_BUFFER_SIZE 	200

#define MQTT_TELEMETRY_SAMPLING 	30*1000 	//time in ms
#define MQTT_STAT_SAMPLING 			600*1000 	//time in ms

#define MQTT_JSON_PAYLOAD_SIZE 		200
#define MQTT_ARRAY_OUTPUT_SIZE 		200

#define MQTT_GROUP          "/klugit"
#define MQTT_PREFIX_TELE    "tele"
#define MQTT_PREFIX_STAT    "stat"
#define MQTT_PREFIX_CMND    "cmnd"
#define MQTT_TYPE_STAT      "/STAT"
#define MQTT_TYPE_SENSOR    "/SENSOR"
#define MQTT_TYPE_BIRTH     "/BIRTH"
#define MQTT_TYPE_VIBRATION "/VIBRATION"
#define MQTT_TYPE_RESULT 	"/RESULT"
#define MQTT_TYPE_STAT 		"/STAT"
#define MQTT_TYPE_WARNING 	"/WARNING"
#define MQTT_TYPE_LIMIT 	"/LIMIT"
#define MQTT_TYPE_INFO 		"/INFO"

#ifdef MQTT_AWS_BROKER
	const char      MQTT_HOST[] = KLUGIT_AWS_MQTT_HOST;
	const uint16_t  MQTT_PORT   = KLUGIT_AWS_MQTT_PORT;
	const char      MQTT_USER[] = "usensores";       // leave blank if no credentials used
	const char      MQTT_PASS[] = "ctrlc#001100";    // leave blank if no credentials used

#else
	const char      MQTT_HOST[] = "test.mosquitto.org";
	const uint16_t  MQTT_PORT   = 1883;
#endif


typedef enum
{
	/** @brief No event */
	MQTT_EVENT_NONE = 0,
	MQTT_EVENT_TELEMETRY_TIMEOUT,
	MQTT_EVENT_TELEMETRY_SENT,
	MQTT_EVENT_STAT_TIMEOUT,    
	MQTT_EVENT_STAT_SENT,
	MQTT_EVENT_UPDATE
} Mqtt_event_t;

typedef enum
{
	/** @brief Initializing the state machine */
	MQTT_STATE_INIT,
	MQTT_STATE_STANDBY,	
	MQTT_STATE_SEND_TELEMETRY,
	MQTT_STATE_SEND_STAT,
    MQTT_STATE_WAIT

} Mqtt_state_t;

typedef enum
{
	/** @brief Initializing the state machine */
	MACHINE_STATE_INIT,
	MACHINE_STATE_OFF,	
	MACHINE_STATE_SMART_MODE_ON,
	MACHINE_STATE_SMART_MODE_OFF,
    MACHINE_STATE_HEAT_NOW_ON

} Machine_state_t;


// PUBLIC PROTOTYPES
void MqttInit();
void MqttTasks();
void MqttSendRelayState();
void MqttSendButtonEvent(bool longpress);
Mqtt_state_t Mqtt_GetState(void);
void MqttSendInfo(uint8_t info_code);
bool Mqtt_is_relay_open();
bool Mqtt_Connect();
void MqttReconnect();


#endif